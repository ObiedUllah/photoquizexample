package ca.abodzay.photoquiz

//Defines a question with multiple options, a correct answer and a corresponding image
class Question(val qAnswers: ArrayList<String>, val qImage: Int, val correctAnswer: String){

}